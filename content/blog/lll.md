+++
title = "Let’s Learn Linux"
date = "2018-01-31T21:49:20+02:00"
tags = ["CSE", "IEDC","ICFOSS","Linux"]
categories = ["Workshops"]
author = "Manjumaney C M"
+++

## FOSSers Club of the College organises one-day workshop on “Let’s Learn Linux” 

![Image](/img/LetsLearnLinux.jpg)

FOSSers Club of the College, in association with the CSE Dept, IEDC VAST and ICFOSS, organised a one day Workshop on “Let’s Learn Linux” in the Programming Lab on 26.01.2018 (Republic Day). FOSSers Club is a an association of Free and Open Source Software (FOSS) enthusiasts of the College. International Centre for Free and Open Source Software (ICFOSS) is an autonomous institution under Govt. of Kerala, mandated with the objectives of coordinating FOSS initiatives in Kerala as well as linking up with such initiatives in other parts of the world.
The morning session of the Workshop was devoted to giving hands-on training on  basic Linux commands and file handling commands. In the afternoon session, system commands and remote administration commands were discussed. Both the sessions were handled by Mr Shali K R, Server Administrator.  There was also a talk by Subin (S2 B Tech (CSE)) on “free software concepts and useful alternative tools”. As many as 43 students from various departments participated in the Workshop.