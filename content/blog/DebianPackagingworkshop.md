+++
title = "Debian Packaging Workshop"
image = ""
date = "2019-08-24T00:00:00+05:30"
tags = ["stall"]
categories = ["events"]
author = "Subin Siby"
aliases = ["/blog/debian-packaging-workshop"]
+++

## **[REGISTER HERE !](https://ee.kobotoolbox.org/single/::CGbHbkeo)**



Hi everyone !

We're happy to announce that we're conducting a Debian Packaging workshop at Vidya on 24th August, 2019 starting at 9:30AM. We're also doing a Debian 10 Buster release party on the same day. It's a public event and anyone can join free.

What's Debian Packaging ?

This is how we install a software in Debian/Ubuntu/Linux Mint etc :

sudo apt install gcompris

That "gcompris" is a package. In this workshop, we'll learn how to make such packages and upload to the main Debian repository which will make your package available in many operating systems such as Ubuntu, Mint, KDE Neon etc.

By making & uploading a package, you'll make a significant contribution to the Debian project.

To know more, feel free to ask in the FOSSers telegram group. Contact : http://fossers.vidyaacademy.ac.in/contact/

Pirate Praveen,Debian Developer,FSCI And Sruthi,Debian Developer will handle the sessions.

> Event On : 2019 August 24th, 9AM
> 
> Event At : [Language Lab(computer centre), Decennial Block (OpenStreetMap Link)](https://www.openstreetmap.org/node/6310148494)

[How to reach Vidya](/contact)

## **[REGISTER HERE !](https://ee.kobotoolbox.org/single/::CGbHbkeo)**

![Poster](/img/blog/wikidata-workshop-poster.png)
